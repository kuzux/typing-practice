import styles from './style.module.css'

document.querySelector<HTMLDivElement>('#app')!.innerHTML = `
  <div class=${styles.container}>
    <p><select id="text-dropdown"></select></p>
    <p class=${styles.display}></p>
    <div class=${styles.stats}>
    </div>
    <button id="restart">Restart</button>
  </div>
  <div class=${styles.overlay}></div>
`
type CharInfo = {
  display_char: string,
  match_char: string,
  status: "pending" | "correct" | "wrong"
  element: HTMLSpanElement,
  offset: number
};

class Typer {
  text: string;
  text_to_match: string;
  p: HTMLParagraphElement;
  stats: HTMLDivElement;
  matched_character_count: number;
  matched_word_count: number;
  wrong_character_count: number;
  start_time: number;
  infos: CharInfo[];
  hide_cursor: number;
  match_cursor: number;
  overlay: HTMLDivElement;

  keydown_handler?: (e: KeyboardEvent) => void = undefined;

  constructor(text: string) {
    this.text = text;
    this.text_to_match = text.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase().replace(/\u0131/g, "i").replace("\u2019", "'").replace(/\s+/g, " ");
    this.p = document.getElementsByClassName(styles.display)[0] as HTMLParagraphElement;
    this.stats = document.getElementsByClassName(styles.stats)[0] as HTMLDivElement;
    this.overlay = document.getElementsByClassName(styles.overlay)[0] as HTMLDivElement;
    this.matched_character_count = 0;
    this.matched_word_count = 0;
    this.wrong_character_count = 0;
    this.start_time = performance.now();
    this.infos = [];
    this.hide_cursor = 0;
    this.match_cursor = 0;

    for (let i=0; i<text.length; i++) {
      let el = document.createElement("span");
      el.textContent = text[i];
      this.infos.push({
        display_char: text[i],
        match_char: this.text_to_match[i],
        status: "pending",
        element: el,
        offset: -1
      });
    }
  }

  update_stats() {
    let curr_time = performance.now();
    let elapsed = curr_time - this.start_time;
    let seconds = elapsed / 1000;
    let minutes = elapsed / 60000;
    let time_display = `${Math.floor(minutes).toString().padStart(2, "0")}:${Math.floor(seconds % 60).toString().padStart(2, "0")}`;

    let chars_per_minute = this.matched_character_count / minutes;
    let words_per_minute = this.matched_word_count / minutes;
    let accuracy_pct = (this.matched_character_count / (this.matched_character_count + this.wrong_character_count)) * 100;
    this.stats.innerHTML = `
      <p>CPM: ${chars_per_minute.toFixed(2)}</p>
      <p>WPM: ${words_per_minute.toFixed(2)}</p>
      <p>Accuracy: ${accuracy_pct.toFixed(2)}%</p>
      <p>Elapsed time: ${time_display}</p>
    `
  }

  on_keydown(e: KeyboardEvent) {
    let key = e.key.toLowerCase();

    let curr = this.infos[this.match_cursor];
    let prev: CharInfo | null = null;
    if(this.match_cursor != 0)
      prev = this.infos[this.match_cursor-1];

    if(key == "backspace" && prev && this.match_cursor > this.hide_cursor) {
      let prev = this.infos[this.match_cursor-1];
      this.match_cursor--;
      if(prev.status == "correct")
        this.matched_character_count--;

      prev.status = "pending";
      prev.element.classList.remove(styles.correct);
      prev.element.classList.remove(styles.wrong);
      return;
    }
    
    if(key.length > 1)
      return;

    if(prev?.status == "wrong")
      return;

    if(this.match_cursor == 0)
      this.start_time = performance.now();
    
    if(key == curr.match_char && curr.status == "pending") {
      curr.element.classList.add(styles.correct);
      this.match_cursor++;
      this.matched_character_count++;
      curr.status = "correct";
      this.update_stats();
    } else {
      this.wrong_character_count++;
      this.match_cursor++;
      curr.element.classList.add(styles.wrong);
      curr.status = "wrong";
      this.update_stats();
      return;
    }

    if(curr.display_char == " ") {
      this.matched_word_count++;
      this.p.style.transform = `translateX(-${curr.offset}px)`;
      for(let i=this.hide_cursor; i<this.match_cursor; i++)
        this.infos[i].element.style.opacity = "0";
      this.hide_cursor = this.match_cursor;
    }
  }

  start() {
    this.update_stats();
    this.p.style.transform = "translateX(0)";
    this.overlay.style.top = `calc(${this.p.offsetTop}px - 1em)`;

    let spans = this.infos.map(info => info.element);
    this.p.replaceChildren(...spans);
    for (let info of this.infos)
      info.offset = info.element.offsetLeft;

    if(this.keydown_handler)
      document.removeEventListener("keydown", this.keydown_handler);
    this.keydown_handler = this.on_keydown.bind(this);
    document.addEventListener("keydown", this.keydown_handler);
  }
}

let typer: Typer | null = null;

async function load_text_file(filename: string) {
  let url = `/${filename}`;
  let res = await fetch(url);
  let text = await res.text();
  typer = new Typer(text.replace(/\s+/g, " "));
  typer.start();
}

function populate_text_dropdown(files: string[], selected?: string) {
  let dropdown = document.getElementById("text-dropdown") as HTMLSelectElement;
  let options = [];
  for(let file of files) {
    let option = document.createElement("option");
    option.value = file;
    option.text = file;
    if(file == selected)
      option.selected = true;
    options.push(option);
  }
  dropdown.replaceChildren(...options);
}

fetch("/public/texts.lst")
  .then(res => res.text())
  .then(async (text: string) => {
    let lines = text.split("\n").filter(l => l.trim().length > 0);

    let idx = Math.floor(Math.random() * lines.length);
    let selected = lines[idx];
    console.log(`Loading ${selected}`);

    populate_text_dropdown(lines, selected);
    await load_text_file(selected);
  });

document.getElementById("restart")!.addEventListener("click", () => {
  typer?.start();
});

document.getElementById("text-dropdown")!.addEventListener("change", async (e) => {
  let filename = (e.target as HTMLSelectElement).value;
  await load_text_file(filename);
});
